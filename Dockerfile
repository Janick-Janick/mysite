FROM ubuntu:18.04

RUN apt-get update &&\
  apt-get install -y --no-install-recommends apt-utils &&\
  apt-get install -y python3-pip python3-dev python3.7 python3.7-dev &&\
  cd /usr/local/bin &&\
  ln -s /usr/bin/python3 python &&\
  python3.7 -m pip install --upgrade pip &&\
  rm -rf /var/lib/apt/lists/*

ARG workdir=/mysite

RUN apt-get update && apt-get install -y git

RUN apt-get install vim -y

RUN apt-get install nginx -y


RUN git clone https://gitlab.com/Janick-Janick/mysite.git $workdir

RUN cd $workdir && pip3.7 install -r requirements.txt &&\
    cp deploy/nginx.conf /etc/nginx/sites-available/mysite_nginx.conf && \
    ln -s /etc/nginx/sites-available/mysite_nginx.conf /etc/nginx/sites-enabled/

RUN echo 'test67'

EXPOSE 8000

CMD ["cmd.sh"]